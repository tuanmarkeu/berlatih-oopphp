<?php
    require_once('animal.php');
    require_once('Ape.php');
    require_once('Frog.php');
    //release 0
    $sheep = new Animal("shaun");

    echo "Sheep name: " . $sheep->name . "<br>"; // "shaun"
    echo "Sheep legs : ". $sheep->legs."<br>"; // 4
    echo "Sheep cold blooded : ". $sheep->cold_blooded."<br> <br>"; // "no"

    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    //release 1
    $sungokong = new Ape("kera sakti");
    echo "Sungokong name : ".$sungokong->name."<br>";
    echo "Sungokong Legs : ".$sungokong->legs."<br>";
    echo "Sungokong cold blooded : ". $sungokong->cold_blooded."<br>";
    echo $sungokong->yell()."<br><br>";// "Auooo"

    $kodok = new Frog("buduk");
    echo "Kodok name : ".$kodok->name."<br>";
    echo "Kodok Legs : ".$kodok->legs."<br>";
    echo "Kodok cold blooded : ". $kodok->cold_blooded."<br>";
    echo $kodok->jump(); // "hop hop"
?>